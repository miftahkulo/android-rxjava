package com.mifhd.rxjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mifhd.rxjava.models.Datasource;
import com.mifhd.rxjava.models.Task;
import com.mifhd.rxjava.util.Logger;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Observable<Task> taskObservable = Observable // create a new Observable object
                .fromIterable(Datasource.createTasksList()) // apply 'fromIterable' operator
                .subscribeOn(Schedulers.io()) // designate worker thread (background)
                .filter(new Predicate<Task>() {
                    @Override
                    public boolean test(Task task) throws Exception {
                        // sleep every item in all items to filtering
                        // try {
                        //     Thread.sleep(2000);
                        // } catch (InterruptedException e) {
                        //     e.printStackTrace();
                        // }
                        // Logger.d("onNext>>: ThreadName>>:" + Thread.currentThread().getName());
                        return task.isComplete();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread()); // designate observer thread (main thread)

        taskObservable.subscribe(new Observer<Task>() {
            @Override
            public void onSubscribe(Disposable d) {
                Logger.d("onSubscribe>>: call");
            }

            @Override
            public void onNext(Task task) {
                Logger.d("onNext>>: ThreadName>>:" + Thread.currentThread().getName());
                Logger.d("onNext>>: task-Description>>:" + task.getDescription());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                Logger.e(e.getMessage());
            }

            @Override
            public void onComplete() {
                Logger.d("onComplete>>:");
            }
        });
    }
}
